import React from 'react'; 
import Header from './Header';
import Order from './Order';
import Inventory from './Inventory';
import SampleFishes from '../sample-fishes';
import Fish from './Fish';
import base from '../base';
class App extends React.Component {
	
	constructor() {
		super();
		this.state = {
			fishes: {},
			order: {}
		}
		this.counter = -1;
		this.addFish = this.addFish.bind(this);
		this.removeFish = this.removeFish.bind(this);
		this.editFish = this.editFish.bind(this);
		this.addOrder = this.addOrder.bind(this);
		this.removeFromOrder = this.removeFromOrder.bind(this);
		this.loadSamples = this.loadSamples.bind(this);
	}

	componentWillMount() {
		
		this.ref = base.syncState(`${this.props.params.storeId}/fishes`,
									{ 
										context: this,
										state: 'fishes' 
									});
		var orders = localStorage.getItem(`order-${this.props.params.storeId}`);
		if(orders){
			this.setState({
				order: JSON.parse(orders)
			})
		}
	}

	shouldComponentUpdate(nextProps, nextState) {
		return true;
	}

	componentWillUnmount() {
		base.removeBinding(this.ref);
	}

	componentWillUpdate(nextProps, nextState) {
		localStorage.setItem(`order-${this.props.params.storeId}`, JSON.stringify(nextState.order));
	}

	addFish(fish) {
		const fishes = {...this.state.fishes};
		const timestamp = Date.now();
		fishes[`fish-${timestamp}`] = fish;
		this.setState({fishes});
	}

	editFish(key, updatedFish) {
		const fishes = {...this.state.fishes};
		fishes[key] = updatedFish;
		this.setState({fishes})
	}

	removeFish(key) {
		const fishes = {...this.state.fishes};
		fishes[key] = null;
		this.setState({fishes});
	}

	loadSamples() {
		this.setState({fishes:SampleFishes});
	}

	addOrder(key) {
		const order = {...this.state.order};
		order[key] = order[key] + 1 || 1;
		this.setState({order});
	}

	removeFromOrder(key) {
		const order = {...this.state.order};
		delete order[key];
		this.setState({order});
	}
	render() {
		return (
			<div className="catch-of-the-day">
				<div className="menu">
					<Header tagline="fresh seafood market"/>
					<ul className="list-of-fish">
					{
						Object
						.keys(this.state.fishes)
						.map(key => <Fish key={key} index={key} details={this.state.fishes[key]} addOrder={this.addOrder} />)
					}
					</ul>
				</div>
				<Order fishes={this.state.fishes} order={this.state.order} removeFromOrder={this.removeFromOrder}/>
				<Inventory addFish={this.addFish} editFish={this.editFish} removeFish={this.removeFish} loadSamples={this.loadSamples} fishes={this.state.fishes}/>
			</div>
		);
	}
}

App.propTypes = {
	params: React.PropTypes.object.isRequired
}
export default App;