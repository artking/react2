import React from 'react';
import AddFishForm from './AddFishForm';
import FishControl from './FishControl';
class Inventory extends React.Component {
	render() {
		return (
			<div className="container">
				<p>Inventory</p>{
					Object
					.keys(this.props.fishes)
					.map(key => <FishControl key={key} fishkey={key} fish={this.props.fishes[key]} editFish={this.props.editFish} removeFish={this.props.removeFish}/>)
				}
				<AddFishForm addFish={this.props.addFish}/>
				<button onClick={this.props.loadSamples}>Load fish list Samples</button>
			</div>	
			)
	}
}

Inventory.propTypes = {
	fishes		: React.PropTypes.object.isRequired,
	addFish 	: React.PropTypes.func.isRequired,
	loadSamples : React.PropTypes.func.isRequired,
	editFish	: React.PropTypes.func.isRequired,
	removeFish	: React.PropTypes.func.isRequired,
}

export default Inventory;