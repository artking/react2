import React from 'react';
import {BrowserRouter, Match, Miss} from 'react-router';
import StorePicker from "./StorePicker";
import App from "./App";
import NoFound from "./NoFound";
const Router = () => {
	return (
			<BrowserRouter baseName="/build">
				<div>
					<Match exactly pattern="/" component={StorePicker} />
					<Match pattern="/store/:storeId" component={App} />
					<Miss component={NoFound} />
				</div>
			</BrowserRouter>
		)
}

export default Router;
