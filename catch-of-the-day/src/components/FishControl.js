import React from 'react';

class FishControl extends React.Component{
	constructor(){
		super();
		this.handleChange = this.handleChange.bind(this);
	}
	handleChange(e, key){
		const fish = this.props.fish;
		const updatedFish = {...fish};
		updatedFish[e.target.name] = e.target.value;
		this.props.editFish(key, updatedFish);
	}
	render(){
		return (
			<div className="fish-edit">
				<input onChange={(e) => this.handleChange(e, this.props.fishkey)} name="name" type="text" value={this.props.fish.name} placeholder="Fish Name"/>
				<input onChange={(e) => this.handleChange(e, this.props.fishkey)} name="price" type="text" value={this.props.fish.price}  placeholder="Fish Price"/>
				<select onChange={(e) => this.handleChange(e, this.props.fishkey)} name="status" value={this.props.fish.status} >
					<option value="available">Fresh!</option>
					<option value="unavailable">Sold out!</option>
				</select>
				<textarea onChange={(e) => this.handleChange(e, this.props.fishkey)} name="desc" value={this.props.fish.desc} placeholder="Fish Desc"></textarea>
				<input onChange={(e) => this.handleChange(e, this.props.fishkey)} name="image" value={this.props.fish.img} type="text" placeholder="Fish Image"/>
				<button onClick={() => this.props.removeFish(this.props.fishkey)}>Remove Fish</button>
			</div>			
			)
	}
}

FishControl.propTypes = {
	editFish	: React.PropTypes.func.isRequired,
	fish 		: React.PropTypes.object.isRequired,
	fishkey 	: React.PropTypes.string.isRequired,
	removeFish  : React.PropTypes.func.isRequired
}

export default FishControl;