import React from 'react';
import {getFunName} from '../helpers';

class StorePicker extends React.Component {
	/*constructor() {
		super();
		this.goToStore = this.goToStore.bind(this);
	}*/
	goToStore (event) {
		event.preventDefault();
		const storeId = this.storeId.value;
		console.log(`redirecting to store ${storeId}`);
		this.context.router.transitionTo(`/store/${storeId}`);
	}
	render () {
		return (
			<form className="store-selector" onSubmit={(e) => this.goToStore(e)} >
				<h1>Please Enter A store</h1>
				<input type="text" defaultValue={getFunName()} required placeholder="Store Name" ref={(input) => this.storeId = input}/>
				<button type="submit">Visit store A -></button>
			</form>
			)
	}
}

StorePicker.contextTypes = {
	router: React.PropTypes.object
}

export default StorePicker;