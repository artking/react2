import React from 'react';
import { formatPrice } from '../helpers';

class Order extends React.Component {
	constructor() {
		super();
		this.renderOrder = this.renderOrder.bind(this);
	}
	renderOrder(key) {
		const fish = this.props.fishes[key];
		const count = this.props.order[key];
		const removeButton = <button onClick={() => this.props.removeFromOrder(key)}>&times;</button>;
		if(!fish || fish.status === 'unavailable') {
			return <li key={key}>No {fish?fish.name:'fish'} sorry {removeButton}</li>
		}
		return (<li key={key}>
					<span>{count}lbs {fish.name} {removeButton}</span>
					<span className="price">{formatPrice(count * fish.price || 0)}</span>
			    </li>)
	}
	render() {
		const fishes = this.props.fishes; 
		const order = this.props.order;
		const orderIds = Object.keys(order);
		const total = orderIds.reduce((subtotal, key) => {
			const fish = fishes[key];
			const isAvailable = fish && fish.status === 'available';
			if(isAvailable) {
				return subtotal + (order[key] * fish["price"] || 0);
			}
			return subtotal;
		}, 0);
		return (
			<div className="order-wrapper">
				<h3>Your Order</h3>
				<ul className="order">
					{orderIds.map(this.renderOrder)}					
					<li className="total">
						<strong> Total: </strong>
						{formatPrice(total)}
					</li>
					
				</ul>
			</div>
			)
	}
}

Order.propTypes = {
	fishes 			:React.PropTypes.object.isRequired,
	order 			:React.PropTypes.object.isRequired,
	removeFromOrder :React.PropTypes.func.isRequired,
}

export default Order;